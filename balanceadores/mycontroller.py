#!/usr/bin/env python2
import argparse
import grpc
import os
import sys
from time import sleep

# Import P4Runtime lib from parent utils dir
sys.path.append(
    os.path.join(os.path.dirname(os.path.abspath(__file__)),
                 '../../utils/'))
import p4runtime_lib.bmv2
import p4runtime_lib.simple_controller
from p4runtime_lib.switch import ShutdownAllSwitchConnections
import p4runtime_lib.helper

# Initially all switches are running the p4 program 1
PROG1 = 0
PROG2 = 1

# Change the p4 program according to this value
TOP_BYTES = 300

pckts_counter = "MyEgress.pckts_counter"

def printGrpcError(e):
    print "gRPC Error:", e.details(),
    status_code = e.code()
    print "(%s)" % status_code.name,
    traceback = sys.exc_info()[2]
    print "[%s:%d]" % (traceback.tb_frame.f_code.co_filename, traceback.tb_lineno)

def select_p4_program(p4info_helper, sw, counter_name, prog):
    pckt_count = 0
    bytes_count = 0
    for index in range(1,4):
        for response in sw.ReadCounters(p4info_helper.get_counters_id(counter_name), index):
            for entity in response.entities:
                counter = entity.counter_entry
                pckt_count += counter.data.packet_count
                bytes_count += counter.data.byte_count
    if pckt_count == 0:
        return prog
    elif bytes_count/pckt_count <= TOP_BYTES:
        return PROG1
    else:
        return PROG2

def load_table_switch(sw_conf, sw, p4info_helper):
    if 'table_entries' in sw_conf:
        table_entries = sw_conf['table_entries']
        for entry in table_entries:
            p4runtime_lib.simple_controller.insertTableEntry(sw, entry, p4info_helper)


def main(p4info_file_path, p4info_file_path2, bmv2_file_path, bmv2_file_path2):

    # Instantiate a P4Runtime helper from the p4info file
    p4info_helper = p4runtime_lib.helper.P4InfoHelper(p4info_file_path)
    p4info_helper2 = p4runtime_lib.helper.P4InfoHelper(p4info_file_path2)

    workdir = os.getcwd()

    # Open and save the control plane rules for all the switches from both programs
    with open("s1-runtime.json", 'r') as sw_conf_file_s1:
        sw1_conf = p4runtime_lib.simple_controller.json_load_byteified(sw_conf_file_s1)
        try:
            p4runtime_lib.simple_controller.check_switch_conf(sw_conf=sw1_conf, workdir=workdir)
        except ConfException as e:
            p4runtime_lib.simple_controller.error("While parsing input runtime configuration: %s" % str(e))
            return
    with open("s2-runtime.json", 'r') as sw_conf_file_s2:
        sw2_conf = p4runtime_lib.simple_controller.json_load_byteified(sw_conf_file_s2)
        try:
            p4runtime_lib.simple_controller.check_switch_conf(sw_conf=sw2_conf, workdir=workdir)
        except ConfException as e:
            p4runtime_lib.simple_controller.error("While parsing input runtime configuration: %s" % str(e))
            return
    with open("s3-runtime.json", 'r') as sw_conf_file_s3:
        sw3_conf = p4runtime_lib.simple_controller.json_load_byteified(sw_conf_file_s3)
        try:
            p4runtime_lib.simple_controller.check_switch_conf(sw_conf=sw3_conf, workdir=workdir)
        except ConfException as e:
            p4runtime_lib.simple_controller.error("While parsing input runtime configuration: %s" % str(e))
            return

    with open("s1-runtime2.json", 'r') as sw_conf_file_s12:
        sw1_conf2 = p4runtime_lib.simple_controller.json_load_byteified(sw_conf_file_s12)
        try:
            p4runtime_lib.simple_controller.check_switch_conf(sw_conf=sw1_conf2, workdir=workdir)
        except ConfException as e:
            p4runtime_lib.simple_controller.error("While parsing input runtime configuration: %s" % str(e))
            return
    with open("s2-runtime2.json", 'r') as sw_conf_file_s22:
        sw2_conf2 = p4runtime_lib.simple_controller.json_load_byteified(sw_conf_file_s22)
        try:
            p4runtime_lib.simple_controller.check_switch_conf(sw_conf=sw2_conf2, workdir=workdir)
        except ConfException as e:
            p4runtime_lib.simple_controller.error("While parsing input runtime configuration: %s" % str(e))
            return
    with open("s3-runtime2.json", 'r') as sw_conf_file_s32:
        sw3_conf2 = p4runtime_lib.simple_controller.json_load_byteified(sw_conf_file_s32)
        try:
            p4runtime_lib.simple_controller.check_switch_conf(sw_conf=sw3_conf2, workdir=workdir)
        except ConfException as e:
            p4runtime_lib.simple_controller.error("While parsing input runtime configuration: %s" % str(e))
            return
    try:
        # Create a switch connection object for s1 and s2;
        # this is backed by a P4Runtime gRPC connection.
        # Also, dump all P4Runtime messages sent to switch to given txt files.
        s1 = p4runtime_lib.bmv2.Bmv2SwitchConnection(
            name='s1',
            address='127.0.0.1:50051',
            device_id=0,
            proto_dump_file='logs/s1-p4runtime-requests.txt')
        s2 = p4runtime_lib.bmv2.Bmv2SwitchConnection(
            name='s2',
            address='127.0.0.1:50052',
            device_id=1,
            proto_dump_file='logs/s2-p4runtime-requests.txt')
        s3 = p4runtime_lib.bmv2.Bmv2SwitchConnection(
            name='s3',
            address='127.0.0.1:50053',
            device_id=2,
            proto_dump_file='logs/s3-p4runtime-requests.txt')

        # Send master arbitration update message to establish this controller as
        # master (required by P4Runtime before performing any other write operation)
        s1.MasterArbitrationUpdate()
        s2.MasterArbitrationUpdate()
        s3.MasterArbitrationUpdate()

        STATE_SW1 = PROG1
        STATE_SW2 = PROG1
        STATE_SW3 = PROG1

        while True:

            # Select program for switch1
            if STATE_SW1 == PROG1:
                res = select_p4_program(p4info_helper, s1, pckts_counter, PROG1)
                if res != PROG1:
                    STATE_SW1 = PROG2
                    # Change program on switch1 to program2
                    s1.SetForwardingPipelineConfig(p4info=p4info_helper2.p4info,
                                                   bmv2_json_file_path=bmv2_file_path2)
                    print "Installed Program 2 on s1"
                    #  Write table entries from json file to switch1
                    load_table_switch(sw1_conf2, s1, p4info_helper2)
            else:
                # STATE_SW1 = PROG2
                res = select_p4_program(p4info_helper2, s1, pckts_counter, PROG2)
                if res != PROG2:
                    STATE_SW1 = PROG1
                    # Change program on switch1 to program1
                    s1.SetForwardingPipelineConfig(p4info=p4info_helper.p4info,
                                                   bmv2_json_file_path=bmv2_file_path)
                    print "Installed Program 1 on s1"
                    #  Write table entries from json file to switch1
                    load_table_switch(sw1_conf, s1, p4info_helper)

            # Select program for switch2
            if STATE_SW2 == PROG1:
                res = select_p4_program(p4info_helper, s2, pckts_counter, PROG1)
                if res != PROG1:
                    STATE_SW2 = PROG2
                    # Change program on switch2 to program2
                    s2.SetForwardingPipelineConfig(p4info=p4info_helper2.p4info,
                                                   bmv2_json_file_path=bmv2_file_path2)
                    print "Installed Program 2 on s2"
                    #  Write table entries from json file to switch2
                    load_table_switch(sw2_conf2, s2, p4info_helper2)
            else:
                # STATE_SW2 = PROG2
                res = select_p4_program(p4info_helper2, s2, pckts_counter, PROG2)
                if res != PROG2:
                    STATE_SW2 = PROG1
                    # Change program on switch2 to program1
                    s2.SetForwardingPipelineConfig(p4info=p4info_helper.p4info,
                                                   bmv2_json_file_path=bmv2_file_path)
                    print "Installed Program 1 on s2"
                    #  Write table entries from json file to switch2
                    load_table_switch(sw2_conf, s2, p4info_helper)

            if STATE_SW3 == PROG1:
                res = select_p4_program(p4info_helper, s3, pckts_counter, PROG1)
                if res != PROG1:
                    STATE_SW3 = PROG2
                    # Change program on switch3 to program2
                    s3.SetForwardingPipelineConfig(p4info=p4info_helper2.p4info,
                                                   bmv2_json_file_path=bmv2_file_path2)
                    print "Installed Program 2 on s3"
                    #  Write table entries from json file to switch3
                    load_table_switch(sw3_conf2, s3, p4info_helper2)
            else:
                # STATE_SW3 = PROG2
                res = select_p4_program(p4info_helper2, s3, pckts_counter, PROG2)
                if res != PROG2:
                    STATE_SW3 = PROG1
                    # Change program on switch3 to program1
                    s3.SetForwardingPipelineConfig(p4info=p4info_helper.p4info,
                                                   bmv2_json_file_path=bmv2_file_path)
                    print "Installed Program 1 on s3"
                    #  Write table entries from json file to switch3
                    load_table_switch(sw3_conf, s3, p4info_helper)

            sleep(5)



    except KeyboardInterrupt:
        print " Shutting down."
    except grpc.RpcError as e:
        printGrpcError(e)

    ShutdownAllSwitchConnections()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='P4Runtime Controller')
    #  p4 info - program 1
    parser.add_argument('--p4info', help='p4info proto in text format from p4c',
                        type=str, action="store", required=False,
                        default='./build/RR.p4.p4info.txt')
    #  p4 info - program 2
    parser.add_argument('--p4info-2', help='p4info proto in text format from p4c -  2',
                        type=str, action="store", required=False,
                        default='./program2/lb_proto.p4.p4info.txt')
    #  bmv2 json file - program 1
    parser.add_argument('--bmv2-json', help='BMv2 JSON file from p4c',
                        type=str, action="store", required=False,
                        default='./build/RR.json')
    #  bmv2 json file - program 2
    parser.add_argument('--bmv2-json-2', help='BMv2 JSON file from p4c - 2',
                        type=str, action="store", required=False,
                        default='./program2/lb_proto.json')
    args = parser.parse_args()

    if not os.path.exists(args.p4info):
        parser.print_help()
        print "\np4info file not found: %s\nHave you run 'make'?" % args.p4info
        parser.exit(1)
    if not os.path.exists(args.bmv2_json):
        parser.print_help()
        print "\nBMv2 JSON file not found: %s\nHave you run 'make'?" % args.bmv2_json
        parser.exit(1)

    main(args.p4info, args.p4info_2, args.bmv2_json, args.bmv2_json_2)
