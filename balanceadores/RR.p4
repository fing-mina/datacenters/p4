/* P4_16 */

#include <core.p4>
#include <v1model.p4> // Architecture for behavioral model (bmv2)

// Constants
const bit<16> ETHERTYPE_IPV4 = 0x800;
const bit<8> PROTO_TCP = 6;
const bit<9> PORT_ZERO = 0;

#define MAX_PORTS 4

/*************************************************************************
*********************** H E A D E R S  ***********************************
*************************************************************************/

header ethernet_t {
	bit<48> dstAddr;
	bit<48> srcAddr;
	bit<16> etherType;
}

header ipv4_t {
	bit<4>  version;
	bit<4>  ihl;
	bit<8>  diffserv;
	bit<16> totalLen;
	bit<16> identification;
	bit<3>  flags;
	bit<13> fragOffset;
	bit<8>  ttl;
	bit<8>  protocol;
	bit<16> hdrChecksum;
	bit<32> srcAddr;
	bit<32> dstAddr;
}

struct metadata { // User defined metadata
	bit<9> selected_port;
}

struct headers {
	ethernet_t ethernet;
	ipv4_t     ipv4;
}

/*************************************************************************
*********************** P A R S E R  ***********************************
*************************************************************************/

parser MyParser(packet_in packet, out headers hdr, inout metadata meta,
                			inout standard_metadata_t standard_metadata) {

	state start {
		transition parse_ethernet;
	}

	// Parsing ethernet header
	state parse_ethernet {
		packet.extract(hdr.ethernet);
		transition select(hdr.ethernet.etherType) {
			    ETHERTYPE_IPV4: parse_ipv4;
			    default: accept; // If etherType is different from IPv4, accept
		}
	}

	// Parsing ipv4 header
	state parse_ipv4 {
		packet.extract(hdr.ipv4);
		transition accept;
	}
}

/*************************************************************************
************   C H E C K S U M    V E R I F I C A T I O N   *************
*************************************************************************/

control MyVerifyChecksum(inout headers hdr, inout metadata meta) {
	apply { }
}

/*************************************************************************
**************  I N G R E S S   P R O C E S S I N G   *******************
*************************************************************************/

control MyIngress(inout headers hdr, inout metadata meta, inout standard_metadata_t standard_metadata) {

	// Extern function defined by v1model, sets a field from the standard metadata with a value indicating
	// that it should be dropped
	action drop() {
		mark_to_drop(standard_metadata);
	}


	register<bit<9>> (MAX_PORTS) reg_last_port;
	bit<9> tmp_last_port;

	action select_port(bit<9> port_base, bit<9> port_max, bit<32> register_id) {
		// The parameters come from the control plane
		// [port_base, port_max] is the range of possible out ports
		// register_id is the id for the register that saves the last port used for the destination

		// Read register for this id, either contains 0 (hasn't been used yet) or contains the last port used for the destination
		reg_last_port.read(tmp_last_port, register_id);

		bit<9> chosen_port = port_base;
		if (tmp_last_port == PORT_ZERO ||  tmp_last_port == port_max) {
			// Either the register for this id hasn't been used, the port to be used is port_base or we need to "restart de round robin"
			// We need to use a field from the user defined metadata (meta.selected_port) in order to access the value (selected port) later

			if (port_base == standard_metadata.ingress_port) { // Do not choose de port from where the packet came
				chosen_port = chosen_port+1;
			}
		} else {
			// Normal RR, we need to select the next port
			chosen_port = tmp_last_port+1;
			if (chosen_port == standard_metadata.ingress_port) {  // Do not choose de port from where the packet came
				if (chosen_port == port_max) { // Choose next port from range
					chosen_port = port_base;
				} else {
					chosen_port = tmp_last_port+1;
				}
			}
		}
		meta.selected_port = chosen_port;
		// Save the value for the next packet
		reg_last_port.write(register_id, chosen_port);
	}

	action select_port_host (bit<9> port_host) {
		// This action sets the port in order to send the message to the connected host
		meta.selected_port = port_host;
	}

	table port_group {
		// Based on the ipv4 dstAddr (lpm) we get the possible out ports and the register number (saves the last
		// port used for Round Robin)
		key = {
			hdr.ipv4.dstAddr: lpm;
		}
		actions = {
			drop;
			select_port;
			select_port_host;
		}
		size = 1024;
		default_action = drop();
	}

	action forward(bit<48> nhop_mac, bit<48>src_mac) {
		standard_metadata.egress_spec = meta.selected_port; // Indicates which outport the packet should go to
		hdr.ethernet.srcAddr = src_mac; // Change ethernet srcAddr to the mac from the selected port
		hdr.ethernet.dstAddr = nhop_mac; // Change ethernet dstAddr to the mac from the next hop
		hdr.ipv4.ttl = hdr.ipv4.ttl - 1; // Decrement ip ttl

	}

	table set_nhop {
		// Forwarding based on the selected port
		key = {
			meta.selected_port: exact;
		}
		actions = {
			forward;
			drop;
		}
		size = 1024;
		default_action = drop();
	}


	apply {
		if (hdr.ipv4.isValid() && hdr.ipv4.ttl > 0) {
			port_group.apply();
			set_nhop.apply();
		}
	}
}

/*************************************************************************
****************  E G R E S S   P R O C E S S I N G   *******************
*************************************************************************/

control MyEgress(inout headers hdr, inout metadata meta, inout standard_metadata_t standard_metadata) {
	// Counter for packets and bytes
	counter(MAX_PORTS, CounterType.packets_and_bytes) pckts_counter;

	apply{
		pckts_counter.count((bit<32>)standard_metadata.egress_port);
	}
}

/*************************************************************************
*************   C H E C K S U M    C O M P U T A T I O N   **************
*************************************************************************/

control MyComputeChecksum(inout headers hdr, inout metadata meta) {
    apply {
    update_checksum(
            hdr.ipv4.isValid(),
            { hdr.ipv4.version,
            hdr.ipv4.ihl,
            hdr.ipv4.diffserv,
            hdr.ipv4.totalLen,
            hdr.ipv4.identification,
            hdr.ipv4.flags,
            hdr.ipv4.fragOffset,
            hdr.ipv4.ttl,
            hdr.ipv4.protocol,
            hdr.ipv4.srcAddr,
            hdr.ipv4.dstAddr },
            hdr.ipv4.hdrChecksum,
            HashAlgorithm.csum16);
    }
}

/*************************************************************************
***********************  D E P A R S E R  *******************************
*************************************************************************/

control MyDeparser(packet_out packet, in headers hdr) {
	apply {
		packet.emit(hdr.ethernet);
		packet.emit(hdr.ipv4);
	}
}

/*************************************************************************
***********************  S W I T C H  *******************************
*************************************************************************/

V1Switch(
MyParser(),
MyVerifyChecksum(),
MyIngress(),
MyEgress(),
MyComputeChecksum(),
MyDeparser()
) main;
