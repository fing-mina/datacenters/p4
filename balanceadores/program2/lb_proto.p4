/* P4_16 */

#include <core.p4>
#include <v1model.p4> // Architecture for behavioral model (bmv2)

// Constants
const bit<16> ETHERTYPE_IPV4 = 0x800;
const bit<8> PROTO_TCP = 6;
const bit<8> PROTO_UDP = 17;

#define MAX_PORTS 4

/*************************************************************************
*********************** H E A D E R S  ***********************************
*************************************************************************/

header ethernet_t {
	bit<48> dstAddr;
	bit<48> srcAddr;
	bit<16> etherType;
}

header ipv4_t {
	bit<4>  version;
	bit<4>  ihl;
	bit<8>  diffserv;
	bit<16> totalLen;
	bit<16> identification;
	bit<3>  flags;
	bit<13> fragOffset;
	bit<8>  ttl;
	bit<8>  protocol;
	bit<16> hdrChecksum;
	bit<32> srcAddr;
	bit<32> dstAddr;
}

struct metadata { // User defined metadata
	bit<9> selected_port;
}

struct headers {
	ethernet_t ethernet;
	ipv4_t     ipv4;
}

/*************************************************************************
*********************** P A R S E R  ***********************************
*************************************************************************/

parser MyParser(packet_in packet, out headers hdr, inout metadata meta,
                			inout standard_metadata_t standard_metadata) {

	state start {
		transition parse_ethernet;
	}

	// Parsing ethernet header
	state parse_ethernet {
		packet.extract(hdr.ethernet);
		transition select(hdr.ethernet.etherType) {
			    ETHERTYPE_IPV4: parse_ipv4;
			    default: accept; // If etherType is different from IPv4, accept
		}
	}

	// Parsing ipv4 header
	state parse_ipv4 {
		packet.extract(hdr.ipv4);
		transition accept;
	}
}

/*************************************************************************
************   C H E C K S U M    V E R I F I C A T I O N   *************
*************************************************************************/

control MyVerifyChecksum(inout headers hdr, inout metadata meta) {
	apply { }
}

/*************************************************************************
**************  I N G R E S S   P R O C E S S I N G   *******************
*************************************************************************/

control MyIngress(inout headers hdr, inout metadata meta, inout standard_metadata_t standard_metadata) {

	// Extern function defined by v1model, sets a field from the standard metadata with a value indicating
	// that it should be dropped
	action drop() {
		mark_to_drop(standard_metadata);
	}

	action forward(bit<9> out_port, bit<48> nhop_mac, bit<48>src_mac) {
		standard_metadata.egress_spec = out_port; // Indicates which outport the packet should go to
		hdr.ethernet.srcAddr = src_mac; // Change ethernet srcAddr to the mac from the selected port
		hdr.ethernet.dstAddr = nhop_mac; // Change ethernet dstAddr to the mac from the next hop
		hdr.ipv4.ttl = hdr.ipv4.ttl - 1; // Decrement ip ttl
	}

    table port_protocol {
        // Choose port according to ip dst and protocol
        key = {
            hdr.ipv4.dstAddr : lpm;
            hdr.ipv4.protocol: exact;
        }
        actions = {
            forward;
            drop;
        }
        size = 1024;
    }

	apply {
		if (hdr.ipv4.isValid() && hdr.ipv4.ttl > 0) {
			port_protocol.apply();
		}
	}
}

/*************************************************************************
****************  E G R E S S   P R O C E S S I N G   *******************
*************************************************************************/

control MyEgress(inout headers hdr, inout metadata meta, inout standard_metadata_t standard_metadata) {
	// Counter for packets and bytes
	counter(MAX_PORTS, CounterType.packets_and_bytes) pckts_counter;

	apply{
		pckts_counter.count((bit<32>)standard_metadata.egress_port);
	}
}

/*************************************************************************
*************   C H E C K S U M    C O M P U T A T I O N   **************
*************************************************************************/

control MyComputeChecksum(inout headers hdr, inout metadata meta) {
    apply {
        update_checksum(
            hdr.ipv4.isValid(),
            { hdr.ipv4.version,
            hdr.ipv4.ihl,
            hdr.ipv4.diffserv,
            hdr.ipv4.totalLen,
            hdr.ipv4.identification,
            hdr.ipv4.flags,
            hdr.ipv4.fragOffset,
            hdr.ipv4.ttl,
            hdr.ipv4.protocol,
            hdr.ipv4.srcAddr,
            hdr.ipv4.dstAddr },
            hdr.ipv4.hdrChecksum,
            HashAlgorithm.csum16);
    }
}

/*************************************************************************
***********************  D E P A R S E R  *******************************
*************************************************************************/

control MyDeparser(packet_out packet, in headers hdr) {
	apply {
		packet.emit(hdr.ethernet);
		packet.emit(hdr.ipv4);
	}
}

/*************************************************************************
***********************  S W I T C H  *******************************
*************************************************************************/

V1Switch(
MyParser(),
MyVerifyChecksum(),
MyIngress(),
MyEgress(),
MyComputeChecksum(),
MyDeparser()
) main;
