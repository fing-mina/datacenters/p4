#!/usr/bin/env python2
import argparse
import grpc
import os
import sys
from time import sleep
import sys

import matplotlib.pyplot as plt
import numpy as np

# Import P4Runtime lib from parent utils dir
sys.path.append(
    os.path.join(os.path.dirname(os.path.abspath(__file__)),
                 '../../utils/'))
import p4runtime_lib.bmv2
from p4runtime_lib.switch import ShutdownAllSwitchConnections
import p4runtime_lib.helper

PCKT_COUNTER_INGRESS = "MyIngress.pckt_counter_ingress"
PCKT_COUNTER_EGRESS = "MyEgress.pckt_counter_egress"

INGRESS_INPUT = 0
EGRESS_INPUT = 1

PACKET_COUNTER_TYPE = 0
BYTE_COUNTER_TYPE = 1
PROM_COUNTER_TYPE = 2

# Creating autocpt arguments
def func(pct, allvalues):
    absolute = int(pct / 100.*np.sum(allvalues))
    return "{:.1f}%\n({:d})".format(pct, absolute)

def graphics(p4info_helper, sw, counter_name, counter_type):
    array = []
    for index in range(1,5):
        for response in sw.ReadCounters(p4info_helper.get_counters_id(counter_name), index):
            for entity in response.entities:
                counter = entity.counter_entry
                if counter_type == PACKET_COUNTER_TYPE:
                    array.append(counter.data.packet_count)
                elif counter_type == BYTE_COUNTER_TYPE:
                    array.append(counter.data.byte_count)
                else:
                    if counter.data.packet_count == 0:
                        array.append(0)
                    else:
                        array.append(counter.data.byte_count/counter.data.packet_count)
    labels = ["Port 1", "Port 2", "Port 3", "Port 4"]
    y = np.array(array)
    plt.pie(y, labels = labels, autopct = lambda pct: func(pct, array))
    inout_title = "Incoming" if counter_name == PCKT_COUNTER_INGRESS else "Outgoing"
    type_counter_title = "packet counter" if counter_type == PACKET_COUNTER_TYPE else "byte counter" if counter_type == BYTE_COUNTER_TYPE else "average packet size"
    plt.title(inout_title + " " + type_counter_title + " on switch " + sw.name)
    plt.show()


def printGrpcError(e):
    print "gRPC Error:", e.details(),
    status_code = e.code()
    print "(%s)" % status_code.name,
    traceback = sys.exc_info()[2]
    print "[%s:%d]" % (traceback.tb_frame.f_code.co_filename, traceback.tb_lineno)

def main(p4info_file_path, sw, counter, counter_type):
    # Instantiate a P4Runtime helper from the p4info file
    p4info_helper = p4runtime_lib.helper.P4InfoHelper(p4info_file_path)

    try:
        # Create a switch connection object for s1 and s2;
        # this is backed by a P4Runtime gRPC connection.
        # Also, dump all P4Runtime messages sent to switch to given txt files.
        s1 = p4runtime_lib.bmv2.Bmv2SwitchConnection(
            name='s1',
            address='127.0.0.1:50051',
            device_id=0,
            proto_dump_file='logs/s1-p4runtime-requests.txt')
        s2 = p4runtime_lib.bmv2.Bmv2SwitchConnection(
            name='s2',
            address='127.0.0.1:50052',
            device_id=1,
            proto_dump_file='logs/s2-p4runtime-requests.txt')
        s3 = p4runtime_lib.bmv2.Bmv2SwitchConnection(
            name='s3',
            address='127.0.0.1:50053',
            device_id=2,
            proto_dump_file='logs/s3-p4runtime-requests.txt')
        s4 = p4runtime_lib.bmv2.Bmv2SwitchConnection(
            name='s4',
            address='127.0.0.1:50054',
            device_id=3,
            proto_dump_file='logs/s4-p4runtime-requests.txt')

        # Send master arbitration update message to establish this controller as
        # master (required by P4Runtime before performing any other write operation)
        s1.MasterArbitrationUpdate()
        s2.MasterArbitrationUpdate()
        s3.MasterArbitrationUpdate()
        s4.MasterArbitrationUpdate()

        # Selected counter (by user)
        selected_counter = PCKT_COUNTER_INGRESS if counter == INGRESS_INPUT else PCKT_COUNTER_EGRESS
        if sw == 1:
            graphics(p4info_helper, s1, selected_counter, counter_type)
        if sw == 2:
            graphics(p4info_helper, s2, selected_counter, counter_type)
        if sw == 3:
            graphics(p4info_helper, s3, selected_counter, counter_type)
        if sw == 4:
            graphics(p4info_helper, s4, selected_counter, counter_type)

    except KeyboardInterrupt:
        print " Shutting down."
    except grpc.RpcError as e:
        printGrpcError(e)

    ShutdownAllSwitchConnections()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='P4Runtime Controller')
    parser.add_argument('--p4info', help='p4info proto in text format from p4c',
                        type=str, action="store", required=False,
                        default='./build/basicMonit.p4.p4info.txt')
    # The user selects the switch to see its data
    parser.add_argument('--sw', help='Selected switch',
                        type=int, action="store", required=False,
                        default=1)
    # The user selects between ingress counter or egress counter
    parser.add_argument('--counter', help='Selected counter (ingress=0/egress=1)',
                        type=int, action="store", required=False,
                        default=INGRESS_INPUT)
    # The user selects between packet counter or byte counter
    parser.add_argument('--type', help='Type of counter (packet=0/byte=1/prom=2)',
                        type=int, action="store", required=False,
                        default=PACKET_COUNTER_TYPE)
    args = parser.parse_args()

    if not os.path.exists(args.p4info):
        parser.print_help()
        print "\np4info file not found: %s\nHave you run 'make'?" % args.p4info
        parser.exit(1)

    main(args.p4info, args.sw, args.counter, args.type)
